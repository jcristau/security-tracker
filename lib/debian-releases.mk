# This file defines the variables describing all Debian repositories
# that need to be fetched in the "update-packages" process

BACKPORT_RELEASES := $(OLDOLDSTABLE) $(OLDSTABLE) $(STABLE)
SECURITY_RELEASES := $(BACKPORT_RELEASES) $(TESTING)
MAIN_RELEASES := $(SECURITY_RELEASES) sid

# Define the variables for the release on the main mirror
define add_main_release =
$(1)_MIRROR = $$(MIRROR)
$(1)_DIST = $(1)
$(1)_ARCHS ?= amd64 arm64 armel armhf hurd-i386 i386 kfreebsd-i386 kfreebsd-amd64 mips mips64el mipsel powerpc ppc64el s390x
$(1)_RELEASE = $(1)
$(1)_SUBRELEASE =
RELEASES += $(1)
endef
$(foreach release,$(MAIN_RELEASES),$(eval $(call add_main_release,$(release))))

# Define the variables for the releases on security.debian.org
define add_security_release =
$(1)_security_MIRROR = $$(SECURITY_MIRROR)
$(1)_security_DIST = $(1)/updates
$(1)_security_ARCHS = $$($(1)_ARCHS)
$(1)_security_RELEASE = $(1)
$(1)_security_SUBRELEASE = security
RELEASES += $(1)_security
endef
$(foreach release,$(SECURITY_RELEASES),$(eval $(call add_security_release,$(release))))

# Define the variables for the *-backports releases
define add_backport_release =
$(1)_backports_MIRROR = $$(MIRROR)
$(1)_backports_DIST = $(1)-backports
$(1)_backports_ARCHS = $$($(1)_ARCHS)
$(1)_backports_RELEASE = $(1)-backports
$(1)_backports_SUBRELEASE =
RELEASES += $(1)_backports
endef
$(foreach release,$(BACKPORT_RELEASES),$(eval $(call add_backport_release,$(release))))
