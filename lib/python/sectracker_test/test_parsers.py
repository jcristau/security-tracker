# Test for sectracker.parsers
# Copyright (C) 2010 Florian Weimer <fw@deneb.enyo.de>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

from sectracker.parsers import *
import sectracker.parsers as p
from sectracker.xpickle import safeunlink, EXTENSION

o = sourcepackages("../../data/packages/sid__main_Sources")
assert type(o) == type({})
assert "bash" in o
assert o["bash"].name == "bash"
assert "bash" in o["bash"].binary

safeunlink("../../data/CVE/list" + EXTENSION)
o = cvelist("../../data/CVE/list")
for err in o.messages:
    print "%s:%d: %s: %s" % (err.file, err.line, err.level, err.message)

safeunlink("../../data/DSA/list" + EXTENSION)
o = dsalist("../../data/DSA/list")
for err in o.messages:
    print "%s:%d: %s: %s" % (err.file, err.line, err.level, err.message)

safeunlink("../../data/DTSA/list" + EXTENSION)
o = dtsalist("../../data/DTSA/list")
for err in o.messages:
    print "%s:%d: %s: %s" % (err.file, err.line, err.level, err.message)

safeunlink("../../data/DLA/list" + EXTENSION)
o = dlalist("../../data/DLA/list")
for err in o.messages:
    print "%s:%d: %s: %s" % (err.file, err.line, err.level, err.message)

Message = sectracker.diagnostics.Message
for (line, res, xmsgs) in [
        (' - foo <unfixed>',
         PackageAnnotation(17, "package", None, "foo", "unfixed", None,
                           None, None, (), False), ()),
        (' - foo',
         PackageAnnotation(17, "package", None, "foo", "unfixed", None,
                           None, None, (), False), ()),
        (' [lenny] - foo <unfixed>',
         PackageAnnotation(17, "package", "lenny", "foo", "unfixed", None,
                           None, None, (), False), ()),
        (' [lenny] - foo <undetermined> (bug #1234)',
         PackageAnnotation(17, "package", "lenny", "foo", "undetermined",
                           None, None, None, (1234,), False), ()),
        (' [lenny] - foo <itp> (bug #1234)',
         PackageAnnotation(17, "package", "lenny", "foo", "itp", None,
                           None, None, (1234,), False), ()),
        (' [lenny] - foo <itp>',
         PackageAnnotation(17, "package", "lenny", "foo", "itp", None,
                           None, None, (), False),
         (Message("CVE", 17, "error",
                  "<itp> needs Debian bug reference"),)),
        (' [lenny] - foo 1.0',
         PackageAnnotation(17, "package", "lenny", "foo", "fixed", "1.0" ,
                           None, None, (), False), ()),
        (' [lenny] - foo <unfixed> (bug filed)',
         PackageAnnotation(17, "package", "lenny", "foo", "unfixed", None,
                           None, None, (), True), ()),
        (' [lenny] - foo <unfixed> (bug filed; bug #1234)',
         PackageAnnotation(17, "package", "lenny", "foo", "unfixed", None,
                           None, None, (1234,), False),
         (Message("CVE", 17, "error",
                  "'bug filed' and bug numbers listed"),)),
        (' [lenny] - foo <unfixed> (low)',
         PackageAnnotation(17, "package", "lenny", "foo", "unfixed", None,
                           None, "low", (), False), ()),
        (' [lenny] - foo <unfixed> (low; low)',
         PackageAnnotation(17, "package", "lenny", "foo", "unfixed", None,
                           None, "low", (), False),
         (Message("CVE", 17, "error", "duplicate flag: 'low'"),)),
        (' [lenny] - foo <unfixed> (bug #1234; garbled)',
         PackageAnnotation(17, "package", "lenny", "foo", "unfixed", None,
                           None, None, (1234,), False),
         (Message("CVE", 17, "error",
                        "invalid inner annotation: 'garbled'"),)),
        (' [lenny] - foo <no-dsa> (explanation goes here)',
         PackageAnnotation(17, "package", "lenny", "foo", "no-dsa", None,
                           "explanation goes here", None, (), False), ()),
        (' [lenny] - foo <end-of-life> (explanation goes here)',
         PackageAnnotation(17, "package", "lenny", "foo", "end-of-life",
                           None, "explanation goes here", None, (), False),
         ()),
        (' [lenny] - foo <not-affected> (explanation goes here)',
         PackageAnnotation(17, "package", "lenny", "foo", "not-affected",
                           None,
                           "explanation goes here", None, (), False), ()),
        ('\t{CVE-2009-1234 CVE-2009-1235}',
         XrefAnnotation(17, "xref",
                        tuple("CVE-2009-1234 CVE-2009-1235".split())),
         ()),
        ('\t{}', None,
         (Message("CVE", 17, "error", "empty cross-reference"),)),
        (' NOT-FOR-US: Plan 9',
         StringAnnotation(17, "NOT-FOR-US", "Plan 9"), ()),
        (' TODO: to-do', StringAnnotation(17, "TODO", "to-do"), ()),
        (' NOTE: note', StringAnnotation(17, "NOTE", "note"), ()),
        (' RESERVED', FlagAnnotation(17, 'RESERVED'), ()),
        (' REJECTED', FlagAnnotation(17, 'REJECTED'), ()),
        (' garbled', None,
         (Message("CVE", 17, "error", "invalid annotation"),)),
        (' [lenny] - foo <garbled> (bug #1234)', None,
         (Message("CVE", 17, "error",
                  "invalid pseudo-version: 'garbled'"),)),
        ]:
    diag = sectracker.diagnostics.Diagnostics()
    diag.setlocation("CVE", 17)
    r = p._annotationdispatcher(line, diag)
    msgs = diag.messages()
    assert tuple(msgs) == xmsgs, repr(msgs)
    assert r == res, repr(r)
