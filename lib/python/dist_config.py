# dist_config.py -- describe how the Debian package database is assembled
# Copyright (C) 2008 Florian Weimer <fw@deneb.enyo.de>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

"""
This Python moule describes how different views of the Debian package
database are assembled from a set of on-disk files.

Each view is labeled by a purpose.  Currently defined purposes are:

  overview: Used to generate the release overview web page.  This
            should not contain vulnerabilities which the security team
            considers processed.

  debsecan: Used to generate the "fix is available" data for debsecan.
            This should reflect the recommended set of sources.list
            entries for the release.
"""

######################################################################
# Configuration section
######################################################################

def apply_config():
    # Invoked at the end of the file.  Edit this to suit your needs.

    common_archs = 'amd64,armel,i386,mips,mipsel,powerpc'.split(',')
    squeeze_archs = common_archs + ['s390','ia64','kfreebsd-amd64','kfreebsd-i386','sparc' ]
    wheezy_archs = [ 'amd64','armel','armhf','i386' ]
    jessie_archs = [ 'amd64','armel','armhf','i386' ]
    stretch_archs = [ 'amd64','arm64','armel','armhf','i386','mips','mips64el','mipsel','ppc64el','s390x' ]
    buster_archs = [ 'amd64','arm64','armel','armhf','i386','mips','mips64el','mipsel','ppc64el','s390x' ]
    sid_archs = [ 'amd64','arm64','armel','armhf','i386','mips','mips64el','mipsel','ppc64el','s390x' ]

    add_release(name='squeeze',
                architectures=squeeze_archs,
                )
    
    add_release(name='wheezy',
                architectures=wheezy_archs,
                )
    
    add_release(name='jessie',
                architectures=jessie_archs,
                )

    add_release(name='stretch',
                architectures=stretch_archs,
                )

    add_release(name='buster',
                architectures=buster_archs,
                )

    add_release(name='sid',
                architectures=sid_archs,
                )

######################################################################
# Support routines
######################################################################

releases = {}

def add_release(name, architectures,
                debsecan_part=('', 'security'),
                overview_part=('', 'security', 'proposed-updates')):
    import debian_support
    name = debian_support.internRelease(name)
    if releases.has_key(name):
        raise ValueError("duplicate release", name)
    releases[name] = {'architectures' : architectures,
                      'purpose' : {'debsecan' : debsecan_part,
                                   'overview' : overview_part}}

# Run the code in the configuration section

apply_config()
del apply_config
